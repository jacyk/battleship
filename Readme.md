# Battleship game
This is one-sided game of Battleship.

Player can make shots to ships, which are placed randomly by computer.

Rules:

*  board size: 10 x 10 squares
*  ships:
    *  Battleship (5 squares) x1
    *  Destroyer (4 squares) x4
*  game ends when all ships are sunk.

# Project requirements
*  ASP .Net Core  2.1
*  web browser (tested on Google Chrome 75.0.3770.100)

License
----

MIT