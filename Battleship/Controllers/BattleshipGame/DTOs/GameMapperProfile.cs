﻿using AutoMapper;
using Battleship.Core.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Controllers.BattleshipGame.DTOs
{
    public class GameMapperProfile : Profile
    {
        public GameMapperProfile()
        {
            this.CreateMap<Core.Game.BattleshipGame, BattleshipGameDto>()
                .ForMember(dst => dst.Status, opts => opts.MapFrom(src => src.GetCurrentStatus()));
            this.CreateMap<Shot, ShotDto>()
                .ForMember(dst => dst.Position, opts => opts.MapFrom(src => src.Position.ToString()));

            this.CreateMap<BattleshipRules, BattleshipRulesDto>()
                .ForMember(dst => dst.Xlist, opts => opts.MapFrom(src => src.GetXlist()))
                .ForMember(dst => dst.Ylist, opts => opts.MapFrom(src => src.GetYlist()));
            this.CreateMap<IBattleshipBoard, BattleshipBoardDto>()
                .ForMember(dst => dst.IsWinner, opts => opts.MapFrom(src => src.IsWinner()));
            this.CreateMap<Ship, ShipDto>();
        }
    }
}
