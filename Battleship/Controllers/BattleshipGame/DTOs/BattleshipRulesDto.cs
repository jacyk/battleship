﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Controllers.BattleshipGame.DTOs
{
    public class BattleshipRulesDto
    {
        public char XcoordinateMin { get; set; }
        public char XcoordinateMax { get; set; }

        public short YcoordinateMin { get; set; }
        public short YcoordinateMax { get; set; }

        public List<char> Xlist { get; set; }

        public List<short> Ylist { get; set; }
    }
}
