﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Controllers.BattleshipGame.DTOs
{
    public class ShipDto
    {
        public string StartPosition { get; set; }
        public ShipType Type { get; set; }
        public ShipOrientation Orientation { get; set; }
    }
}
