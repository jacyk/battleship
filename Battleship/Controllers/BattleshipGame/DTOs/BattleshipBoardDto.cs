﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Controllers.BattleshipGame.DTOs
{
    public class BattleshipBoardDto
    {
        public string PlayerName { get; set; }

        public BattleshipRulesDto Rules { get; set; }

        public List<ShipDto> Ships { get; set; }
        public List<ShotDto> Shots { get; set; }

        public bool IsWinner { get; set; }
    }
}
