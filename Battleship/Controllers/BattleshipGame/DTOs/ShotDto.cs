﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Controllers.BattleshipGame.DTOs
{
    public class ShotDto
    {
        public string Position { get; set; }
        public ShipDto ShipHit { get; set; }
        public ShotResult Result { get; set; }
    }
}
