﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Battleship.Controllers.BattleshipGame.DTOs
{
    public class BattleshipGameDto
    {
        public Guid Id { get; set; }

        public BattleshipBoardDto Player1Board { get; set; }
        public BattleshipBoardDto Player2Board { get; set; }

        public BattleshipGameStatus Status { get; set; }
    }
}
