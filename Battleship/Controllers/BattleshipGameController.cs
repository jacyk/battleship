﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Battleship.Controllers.BattleshipGame.DTOs;
using Battleship.Core.Game;
using Battleship.Core.Game.Factories;
using Battleship.DataStorage.Repositories;
using Microsoft.AspNetCore.Mvc;
using BattleshipCore = Battleship.Core.Game;

namespace Battleship.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BattleshipGameController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<BattleshipCore.BattleshipGame> _gameRepo;
        private readonly ICoordinatesFactory _coordinatesFactory;
        private readonly IBattleshipGameFactory _gameFactory;

        public BattleshipGameController(IMapper mapper, IRepository<BattleshipCore.BattleshipGame> gameRepo, ICoordinatesFactory coordinatesFactory, IBattleshipGameFactory gameFactory)
        {
            this._mapper = mapper;
            this._gameRepo = gameRepo;
            this._coordinatesFactory = coordinatesFactory;
            this._gameFactory = gameFactory;
        }

        [HttpPost("[action]")]
        public BattleshipGameDto CreateNewGame(string playerName)
        {
            BattleshipCore.BattleshipGame newGame = this._gameFactory.CreateStandardGameWithComputerPlayer(playerName);
            this._gameRepo.Add(newGame);

            BattleshipGameDto newGameOutput = this._mapper.Map<BattleshipGameDto>(newGame);
            return newGameOutput;
        }

        [HttpGet("[action]")]
        public BattleshipGameDto GetGame(Guid gameId)
        {
            BattleshipCore.BattleshipGame game = this._gameRepo.Get(gameId);

            BattleshipGameDto gameOutput = this._mapper.Map<BattleshipGameDto>(game);
            return gameOutput;
        }

        [HttpPost("[action]")]
        public ShotDto MakeShot(Guid gameId, string xy)
        {
            BattleshipCore.BattleshipGame game = this._gameRepo.Get(gameId);
            CoordinatesVO shotPosition = this._coordinatesFactory.Create(xy);
            Shot shot = game.Player2Board.MakeShot(shotPosition, game.Player1Board);

            ShotDto shotOutput = this._mapper.Map<ShotDto>(shot);
            return shotOutput;
        }
    }
}