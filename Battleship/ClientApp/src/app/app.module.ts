import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ServiceProxiesModule } from '../shared/service-proxies/service-proxies.module'

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { BattleshipComponent } from './battleship/battleship.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    BattleshipComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: BattleshipComponent, pathMatch: 'full' }
    ]),
    ServiceProxiesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
