import { Component, Inject, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms'
import { HttpClient } from '@angular/common/http';
import { GameService, GameDto, BattleshipGameStatus, BattleshipRulesDto, CoordinateDto, ShotResult } from '../../shared/service-proxies/battleship-game.service'

class Square {
  xy: string;
  shotResult: ShotResult;
}

class SquaresRow {
  squares: Square[];
}

@Component({
  selector: 'app-battleship',
  templateUrl: './battleship.component.html',
})
export class BattleshipComponent implements OnInit {
  private _gameSvc: GameService;

  public newGameForm: FormGroup;

  public game: GameDto;
  public xList: string[];
  public yList: number[];
  public squaresRows: SquaresRow[];

  private _isMakingShotPending;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, gameSvc: GameService) {
    this._gameSvc = gameSvc;
  }

  ngOnInit() {
    this.form();
  }

  form() {
    this.newGameForm = new FormGroup({
      'playerName': new FormControl(null, [
        Validators.required
      ])
    });
  };

  private createSquaresBoardFromSquaresList(rules: BattleshipRulesDto): void {

    this.xList = rules.xlist;
    this.yList = rules.ylist;

    this.squaresRows = [];

    for (let i = 0; i < this.yList.length; i++) {
      let squaresRow = new SquaresRow();
      squaresRow.squares = [];

      for (let j = 0; j < this.xList.length; j++) {
        let square = new Square();
        square.xy = this.xList[j] + this.yList[i].toString();
        square.shotResult = ShotResult.unknown;

        squaresRow.squares.push(square);
      }

      this.squaresRows.push(squaresRow);
    }
  };

  isGameInitialized(): boolean {
    return (this.game != null);
  };

  isGameFinished(): boolean {
    return (this.game && this.game.status == BattleshipGameStatus.finished);
  };

  createNewGame(): void {
    this._gameSvc.createNewGame(this.playerName.value).subscribe(res => {
      this.game = res;

      this.createSquaresBoardFromSquaresList(this.game.player1Board.rules);
    });
  };

  refreshGame(): void {
    this._gameSvc.getGame(this.game.id).subscribe(res => this.game = res);
  };

  makeShot(square: Square): void {
    this._isMakingShotPending = true;

    this._gameSvc.makeShot(this.game.id, square.xy).subscribe(
      res => {
        square.shotResult = res.result;
        this.refreshGame();
        this._isMakingShotPending = false;
      },
      error => {
        this._isMakingShotPending = false;
      }
    );
  };

  get playerName() { return this.newGameForm.get('playerName'); };

  isSquareDisabled(square: Square): boolean {
    let isSquareDisabled = (!(square && square.shotResult == ShotResult.unknown) || this._isMakingShotPending);
    return isSquareDisabled;
  };

  getSquareClass(square: Square) {
    let squareClass: string;

    if (square.shotResult == ShotResult.unknown) {
      squareClass = 'glyphicon glyphicon glyphicon-stop';
    } else if (square.shotResult == ShotResult.miss) {
      squareClass = 'glyphicon glyphicon-minus-sign';
    } else if (square.shotResult == ShotResult.hit) {
      squareClass = 'glyphicon glyphicon-plus';
    } else if (square.shotResult == ShotResult.hitAndSink) {
      squareClass = 'glyphicon glyphicon-plus';
    }

    return squareClass;
  }

  getWinnerName(): string {
    let winnerName: string;

    if (this.game) {
      if (this.game.player1Board.isWinner) {
        winnerName = this.game.player1Board.playerName;
      } else if (this.game.player2Board.isWinner) {
        winnerName = this.game.player2Board.playerName;
      }
    }

    return winnerName;
  }
}


