import { NgModule } from '@angular/core';

import * as ApiServiceProxies from './battleship-game.service';

@NgModule({
  providers: [
    ApiServiceProxies.GameService
  ]
})
export class ServiceProxiesModule { }
