import { Observable } from 'rxjs/Observable'
import { Injectable, Inject } from '@angular/core'
import { HttpClient } from '@angular/common/http';

interface ICoordinateDto {
  xy: string;
};

export class CoordinateDto implements ICoordinateDto {
  xy: string;
};

export enum ShotResult {
  unknown = 0,

  miss = 1,
  hit = 2,
  hitAndSink = 3
}

interface IShotDto {
  position: string;
  shipHit: ShipDto;
  result: ShotResult;
  resultText: string;
}

export class ShotDto implements IShotDto {
  position: string;
  shipHit: ShipDto;
  result: ShotResult;
  resultText: string;
}

export enum ShipType {
  destroyer = 2,
  battleship = 5
}

export enum ShipOrientation {
  horizontal,
  vertical
};

interface IShipDto {
  startPosition: string;
  type: ShipType;
  typeText: string;
  orientation: ShipOrientation;
  orientationText: string;
};

export class ShipDto implements IShipDto {
  startPosition: string;
  type: ShipType;
  typeText: string;
  orientation: ShipOrientation;
  orientationText: string;
};

interface IBattleshipRulesDto {
  xcoordinateMin: number;
  xcoordinateMax: number;

  ycoordinateMin: number;
  ycoordinateMax: number;
};

export class BattleshipRulesDto implements IBattleshipRulesDto {
  xcoordinateMin: number;
  xcoordinateMax: number;

  ycoordinateMin: number;
  ycoordinateMax: number;

  xlist: string[];
  ylist: number[];
};

interface IBattleshipBoardDto {
  playerName: string;

  ships: ShipDto[];
  shots: ShotDto[];

  isWinner: boolean;
};

export class BattleshipBoardDto implements IBattleshipBoardDto {
  playerName: string;

  rules: BattleshipRulesDto;

  ships: ShipDto[];
  shots: ShotDto[];

  isWinner: boolean;
};

export enum BattleshipGameStatus {
  unknown = 0,

  duringPreparation = 1,
  duringGame = 2,
  finished = 3
};

interface IGameDto {
  id: string;
  player1Board: BattleshipBoardDto;
  player2Board: BattleshipBoardDto;
  status: BattleshipGameStatus;
  statusText: string;
};

export class GameDto implements IGameDto {
  id: string;
  player1Board: BattleshipBoardDto;
  player2Board: BattleshipBoardDto;
  status: BattleshipGameStatus;
  statusText: string;
};

@Injectable()
export class GameService {
  private _http: HttpClient;
  private _baseUrl: string;

  constructor(@Inject(HttpClient) http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    this._http = http;
    this._baseUrl = baseUrl ? baseUrl : "";
  }

  createNewGame(playerName: string): Observable<GameDto> {
    return this._http.post<GameDto>(this._baseUrl + 'api/BattleshipGame/CreateNewGame', null, { params: { playerName: playerName } });
  };

  getGame(gameId: string): Observable<GameDto> {
    return this._http.get<GameDto>(this._baseUrl + 'api/BattleshipGame/GetGame', { params: { gameId: gameId } });
  };

  makeShot(gameId: string, xy: string): Observable<ShotDto> {
    return this._http.post<ShotDto>(this._baseUrl + 'api/BattleshipGame/MakeShot', null, { params: { gameId: gameId, xy: xy } });
  };
}
