﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.DataStorage
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
