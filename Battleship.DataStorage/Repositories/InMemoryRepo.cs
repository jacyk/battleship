﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.DataStorage.Repositories
{
    public class InMemoryRepo<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        private static Dictionary<TKey, TEntity> _storage { get; set; }

        public InMemoryRepo()
        {
            if (_storage == null)
                _storage = new Dictionary<TKey, TEntity>();
        }

        public TEntity Get(TKey key)
        {
            TEntity entity;
            _storage.TryGetValue(key, out entity);
            return entity;
        }

        public bool Add(TEntity entity)
        {
            bool isEntityAdded;

            if (!_storage.ContainsKey(entity.Id))
            {
                _storage.Add(entity.Id, entity);
                isEntityAdded = true;
            }
            else
                isEntityAdded = false;

            return isEntityAdded;
        }

        public bool Update(TEntity entity)
        {
            bool isEntityUpdated;

            if (_storage.ContainsKey(entity.Id))
            {
                _storage.Remove(entity.Id);
                isEntityUpdated = this.Add(entity);
            }
            else
                isEntityUpdated = false;

            return isEntityUpdated;
        }

        public bool Delete(TEntity entity)
        {
            bool isEntityDeleted;

            if (_storage.ContainsKey(entity.Id))
            {
                _storage.Remove(entity.Id);
                isEntityDeleted = true;
            }
            else
                isEntityDeleted = false;

            return isEntityDeleted;
        }
    }

    public class InMemoryRepo<TEntity> : InMemoryRepo<TEntity, Guid>, IRepository<TEntity>
        where TEntity : IEntity<Guid>
    {
    }
}
