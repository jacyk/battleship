﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.DataStorage.Repositories
{
    public interface IRepository<TEntity> : IRepository<TEntity, Guid>
        where TEntity : IEntity<Guid>
    {

    }

    public interface IRepository<TEntity, TKey>
        where TEntity : IEntity<TKey>
    {
        TEntity Get(TKey key);
        bool Add(TEntity entity);
        bool Update(TEntity entity);
        bool Delete(TEntity entity);
    }
}
