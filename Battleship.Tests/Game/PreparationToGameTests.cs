﻿using Battleship.Core.Game.Factories;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Battleship.Core.Game;
using Battleship.Core.Game.Enums;
using Shouldly;
using Battleship.Core.Game.Exceptions;
using Battleship.Core.Game.Validators;

namespace Battleship.Tests.Game
{
    public static class PreparationToGameExtensions
    {
        private static ICoordinatesFactory _coordinatesFactory { get { return new CoordinatesFactory(); } }
        private static IShipValidator _shipValidator { get { return new ShipValidator(); } }
        private static IShotValidator _shotValidator { get { return new ShotValidator(); } }
        private static IShipFactory _shipFactory { get { return new ShipFactory(_coordinatesFactory, _shipValidator); } }
        private static IBattleshipRulesFactory _battleshipRulesFactory { get { return new BattleshipRulesFactory(); } }

        public static IBattleshipBoard CreateStandardGameBoard()
        {
            BattleshipRules standardRules = _battleshipRulesFactory.CreateRulesForTwoPlayersMode();

            IBattleshipBoard gameBoard = new BattleshipBoard(_shipValidator, _shotValidator, standardRules);
            return gameBoard;
        }

        public static bool PlaceShip(this IBattleshipBoard gameBoard, string startingPoint, ShipType shipType, ShipOrientation shipOrientation)
        {
            Ship ship = _shipFactory.CreateShip(startingPoint, shipType, shipOrientation);
            bool isShipPlaced = gameBoard.PlaceShip(ship);

            return isShipPlaced;
        }

        public static bool PlaceSampleDestroyer1OnNotOccupiedSquares(this IBattleshipBoard gameBoard)
        {
            return gameBoard.PlaceShip("B6", ShipType.Destroyer, ShipOrientation.Horizontal);
        }

        public static bool PlaceSampleDestroyer2OnNotOccupiedSquares(this IBattleshipBoard gameBoard)
        {
            return gameBoard.PlaceShip("G4", ShipType.Destroyer, ShipOrientation.Vertical);
        }

        public static bool PlaceSampleDestroyer3OnNotOccupiedSquares(this IBattleshipBoard gameBoard)
        {
            return gameBoard.PlaceShip("E1", ShipType.Destroyer, ShipOrientation.Horizontal);
        }

        public static bool PlaceSampleBattleship1OnNotOccupiedSquares(this IBattleshipBoard gameBoard)
        {
            return gameBoard.PlaceShip("I6", ShipType.Battleship, ShipOrientation.Vertical);
        }

        public static bool PlaceSampleBattleship2OnNotOccupiedSquares(this IBattleshipBoard gameBoard)
        {
            return gameBoard.PlaceShip("A3", ShipType.Battleship, ShipOrientation.Horizontal);
        }

        public static bool PlaceSampleShipOutsideGrid(this IBattleshipBoard gameBoard)
        {
            return gameBoard.PlaceShip("G1", ShipType.Battleship, ShipOrientation.Horizontal);
        }

        public static bool PlaceSampleShipWhichOverlapWithSampleDestroyer1(this IBattleshipBoard gameBoard)
        {
            return gameBoard.PlaceShip("F7", ShipType.Destroyer, ShipOrientation.Vertical);
        }
    }

    public class PreparationToGameTests
    {
        private const string SAMPLE_PLAYER_NAME = "John";
        
        [Fact]
        public void WhenNewGameIsStarted_AndPlayerNameIsEntered_ThenPreparationToGameIsStarted()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();

            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);

            gameBoard.GetCurrentStatus().ShouldBe(BattleshipBoardStatus.DuringPreparation);
            gameBoard.IsEmpty().ShouldBeTrue();
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesShipOnNotOccupiedSquares_ThenShipIsAddedToGame()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);

            bool isShipPlaced = gameBoard.PlaceSampleDestroyer1OnNotOccupiedSquares();

            isShipPlaced.ShouldBeTrue();
            gameBoard.GetShipsCount().ShouldBe(1);
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesShipOutsideGrid_ThenShipIsNotAddedToGame_AndThrowsException()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);

            bool isShipPlaced = false;
            BattleshipGameException exception = null;
            try
            {
                isShipPlaced = gameBoard.PlaceSampleShipOutsideGrid();
            }
            catch(BattleshipGameException ex)
            {
                exception = ex;
            }
            
            isShipPlaced.ShouldBeFalse();
            gameBoard.GetShipsCount().ShouldBe(0);
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<BattleshipGameException>();
            exception.Message.ShouldBe("Unable to place ship, because it can not fit on the board");
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesShipWhichOverlap_ThenShipIsNotAddedToGame_AndThrowsException()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);
            gameBoard.PlaceSampleDestroyer1OnNotOccupiedSquares();

            bool isShipPlaced = false;
            BattleshipGameException exception = null;
            try
            {
                isShipPlaced = gameBoard.PlaceSampleShipWhichOverlapWithSampleDestroyer1();
            }
            catch (BattleshipGameException ex)
            {
                exception = ex;
            }

            isShipPlaced.ShouldBeFalse();
            gameBoard.GetShipsCount().ShouldBe(1);
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<BattleshipGameException>();
            exception.Message.ShouldBe("Unable to place ship, because it overlap with other ship");
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesFirstBattleship_ThenShipIsAddedToGame()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);

            bool isShipPlaced = gameBoard.PlaceSampleBattleship1OnNotOccupiedSquares();

            isShipPlaced.ShouldBeTrue();
            gameBoard.GetShipsCount().ShouldBe(1);
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesSecondBattleship_ThenShipIsNotAddedToGame_AndThrowsException()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);
            gameBoard.PlaceSampleBattleship1OnNotOccupiedSquares();

            bool isShipPlaced = false;
            BattleshipGameException exception = null;
            try
            {
                isShipPlaced = gameBoard.PlaceSampleBattleship2OnNotOccupiedSquares();
            }
            catch (BattleshipGameException ex)
            {
                exception = ex;
            }

            isShipPlaced.ShouldBeFalse();
            gameBoard.GetShipsCount().ShouldBe(1);
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<BattleshipGameException>();
            exception.Message.ShouldBe("Unable to place ship, because all available ships of type 'Battleship' are already placed on board");
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesFirstTwoDestroyers_ThenShipIsAddedToGame()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);

            bool isFirstShipPlaced = gameBoard.PlaceSampleDestroyer1OnNotOccupiedSquares();
            bool isSecondShipPlaced = gameBoard.PlaceSampleDestroyer2OnNotOccupiedSquares();

            isFirstShipPlaced.ShouldBeTrue();
            isSecondShipPlaced.ShouldBeTrue();
            gameBoard.GetShipsCount().ShouldBe(2);
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesThirdDestroyer_ThenShipIsNotAddedToGame_AndThrowsException()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);
            gameBoard.PlaceSampleDestroyer1OnNotOccupiedSquares();
            gameBoard.PlaceSampleDestroyer2OnNotOccupiedSquares();

            bool isShipPlaced = false;
            BattleshipGameException exception = null;
            try
            {
                isShipPlaced = gameBoard.PlaceSampleDestroyer3OnNotOccupiedSquares();
            }
            catch (BattleshipGameException ex)
            {
                exception = ex;
            }

            isShipPlaced.ShouldBeFalse();
            gameBoard.GetShipsCount().ShouldBe(2);
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<BattleshipGameException>();
            exception.Message.ShouldBe("Unable to place ship, because all available ships of type 'Destroyer' are already placed on board");
        }

        [Fact]
        public void WhenPlayerIsDuringPreparationToGame_AndPlacesAllShipsOnNotOccupiedSquares_ThenShipsAreAddedToGame_AndPlayerIsReadyToStartGame()
        {
            IBattleshipBoard gameBoard = PreparationToGameExtensions.CreateStandardGameBoard();
            gameBoard.SetPlayerName(SAMPLE_PLAYER_NAME);
            gameBoard.PlaceSampleBattleship1OnNotOccupiedSquares();
            gameBoard.PlaceSampleDestroyer1OnNotOccupiedSquares();

            bool isShipPlaced = gameBoard.PlaceSampleDestroyer2OnNotOccupiedSquares();

            isShipPlaced.ShouldBeTrue();
            gameBoard.GetShipsCount().ShouldBe(3);
            gameBoard.GetCurrentStatus().ShouldBe(BattleshipBoardStatus.DuringGame);
        }
    }
}
