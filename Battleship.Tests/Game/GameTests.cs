﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using Battleship.Core.Game.Factories;
using Battleship.Core.Game;
using Battleship.Core.Game.Enums;
using Battleship.Core.Game.Exceptions;
using Battleship.Core.Game.Validators;

namespace Battleship.Tests.Game
{
    public static class GameExtensions
    {
        private static ICoordinatesFactory _coordinatesFactory { get { return new CoordinatesFactory(); } }
        private static IShipValidator _shipValidator { get { return new ShipValidator(); } }
        private static IShotValidator _shotValidator { get { return new ShotValidator(); } }
        private static IShipFactory _shipFactory { get { return new ShipFactory(_coordinatesFactory, _shipValidator); } }
        private static IBattleshipRulesFactory _battleshipRulesFactory { get { return new BattleshipRulesFactory(); } }
        private static IBattleshipGameFactory _gameFactory { get { return new BattleshipGameFactory(_battleshipRulesFactory, _shipValidator, _shotValidator, _shipFactory); } }

        public static IBattleshipGame CreateStandardGame()
        {
            IBattleshipGame game = _gameFactory.CreateGame();
            return game;
        }

        public static bool PlaceShipForPlayer1(this IBattleshipGame game, string startingPoint, ShipType shipType, ShipOrientation shipOrientation)
        {
            Ship ship = _shipFactory.CreateShip(startingPoint, shipType, shipOrientation);
            bool isShipPlaced = game.Player1Board.PlaceShip(ship);

            return isShipPlaced;
        }

        public static bool PlaceShipForPlayer2(this IBattleshipGame game, string startingPoint, ShipType shipType, ShipOrientation shipOrientation)
        {
            Ship ship = _shipFactory.CreateShip(startingPoint, shipType, shipOrientation);
            bool isShipPlaced = game.Player2Board.PlaceShip(ship);

            return isShipPlaced;
        }

        public static bool PlaceSampleDestroyer1ForPlayer1OnNotOccupiedSquares(this IBattleshipGame game)
        {
            return game.PlaceShipForPlayer1("B3", ShipType.Destroyer, ShipOrientation.Horizontal);
        }

        public static bool PlaceSampleDestroyer2ForPlayer1OnNotOccupiedSquares(this IBattleshipGame game)
        {
            return game.PlaceShipForPlayer1("D5", ShipType.Destroyer, ShipOrientation.Vertical);
        }
        
        public static bool PlaceSampleBattleship1ForPlayer1OnNotOccupiedSquares(this IBattleshipGame game)
        {
            return game.PlaceShipForPlayer1("J1", ShipType.Battleship, ShipOrientation.Vertical);
        }

        public static void PreparePlayer1BoardToGame(this IBattleshipGame game, string playerName)
        {
            BattleshipRules computerPlayerRules = _battleshipRulesFactory.CreateRulesForTwoPlayersMode();

            game.SetFirstPlayerBoard(playerName, computerPlayerRules);
            game.PlaceSampleDestroyer1ForPlayer1OnNotOccupiedSquares();
            game.PlaceSampleDestroyer2ForPlayer1OnNotOccupiedSquares();
            game.PlaceSampleBattleship1ForPlayer1OnNotOccupiedSquares();
        }

        public static bool PlaceSampleDestroyer1ForPlayer2OnNotOccupiedSquares(this IBattleshipGame game)
        {
            return game.PlaceShipForPlayer2("I7", ShipType.Destroyer, ShipOrientation.Vertical);
        }

        public static bool PlaceSampleDestroyer2ForPlayer2OnNotOccupiedSquares(this IBattleshipGame game)
        {
            return game.PlaceShipForPlayer2("G7", ShipType.Destroyer, ShipOrientation.Vertical);
        }
        
        public static bool PlaceSampleBattleship1ForPlayer2OnNotOccupiedSquares(this IBattleshipGame game)
        {
            return game.PlaceShipForPlayer2("J1", ShipType.Battleship, ShipOrientation.Vertical);
        }

        public static void PreparePlayer2BoardToGame(this IBattleshipGame game, string playerName)
        {
            BattleshipRules humanPlayerRules = _battleshipRulesFactory.CreateRulesForTwoPlayersMode();

            game.SetSecondPlayerBoard(playerName, humanPlayerRules);
            game.PlaceSampleDestroyer1ForPlayer2OnNotOccupiedSquares();
            game.PlaceSampleDestroyer2ForPlayer2OnNotOccupiedSquares();
            game.PlaceSampleBattleship1ForPlayer2OnNotOccupiedSquares();
        }

        public static Shot MakeShotForPlayer2(this IBattleshipGame game, string shotPositionXY)
        {
            CoordinatesVO shotPosition = _coordinatesFactory.Create(shotPositionXY);

            Shot madeShot = game.Player2Board.MakeShot(shotPosition, game.Player1Board);
            return madeShot;
        }

        public static Shot MakeSampleShotOnEmptySquareForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("A6");
        }

        public static Shot MakeSampleShotOnSquareWithPart1OfSampleDestroyer1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("B3");
        }

        public static Shot MakeSampleShotOnSquareWithPart2OfSampleDestroyer1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("C3");
        }

        public static Shot MakeSampleShotOnSquareWithPart3OfSampleDestroyer1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("D3");
        }

        public static Shot MakeSampleShotOnSquareWithPart4OfSampleDestroyer1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("E3");
        }

        public static Shot MakeSampleShotOnSquareWithPart1OfSampleDestroyer2ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("D5");
        }

        public static Shot MakeSampleShotOnSquareWithPart2OfSampleDestroyer2ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("D6");
        }

        public static Shot MakeSampleShotOnSquareWithPart3OfSampleDestroyer2ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("D7");
        }

        public static Shot MakeSampleShotOnSquareWithPart4OfSampleDestroyer2ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("D8");
        }

        public static Shot MakeSampleShotOnSquareWithPart1OfSampleBattleship1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("J1");
        }

        public static Shot MakeSampleShotOnSquareWithPart2OfSampleBattleship1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("J2");
        }

        public static Shot MakeSampleShotOnSquareWithPart3OfSampleBattleship1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("J3");
        }

        public static Shot MakeSampleShotOnSquareWithPart4OfSampleBattleship1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("J4");
        }

        public static Shot MakeSampleShotOnSquareWithPart5OfSampleBattleship1ForPlayer2(this IBattleshipGame game)
        {
            return game.MakeShotForPlayer2("J5");
        }
    }

    public class GameTests
    {
        private readonly IBattleshipRulesFactory _battleshipRulesFactory;

        public GameTests()
        {
            this._battleshipRulesFactory = new BattleshipRulesFactory();
        }

        private const string SAMPLE_PLAYER1_NAME = "John";
        private const string SAMPLE_PLAYER2_NAME = "Jacob";

        [Fact]
        public void WhenFirstPlayerHasAllShipsPlaced_AndSecondPlayerPlaceLastShip_ThenGameStarts()
        {
            IBattleshipGame game = GameExtensions.CreateStandardGame();

            game.PreparePlayer1BoardToGame(SAMPLE_PLAYER1_NAME);

            BattleshipRules humanPlayerRules = _battleshipRulesFactory.CreateRulesForTwoPlayersMode();

            game.SetSecondPlayerBoard(SAMPLE_PLAYER2_NAME, humanPlayerRules);
            game.PlaceSampleDestroyer1ForPlayer2OnNotOccupiedSquares();
            game.PlaceSampleDestroyer2ForPlayer2OnNotOccupiedSquares();

            bool isShipPlaced = game.PlaceSampleBattleship1ForPlayer2OnNotOccupiedSquares();

            isShipPlaced.ShouldBeTrue();
            game.GetCurrentStatus().ShouldBe(BattleshipGameStatus.DuringGame);
        }

        [Fact]
        public void WhenGameIsNotStarted_AndMakeShot_ThenThrowsException()
        {
            IBattleshipGame game = GameExtensions.CreateStandardGame();

            game.PreparePlayer1BoardToGame(SAMPLE_PLAYER1_NAME);

            BattleshipRules humanPlayerRules = _battleshipRulesFactory.CreateRulesForTwoPlayersMode();

            game.SetSecondPlayerBoard(SAMPLE_PLAYER2_NAME, humanPlayerRules);
            game.PlaceSampleDestroyer1ForPlayer2OnNotOccupiedSquares();
            game.PlaceSampleDestroyer2ForPlayer2OnNotOccupiedSquares();

            Shot madeShot = null;
            Exception exception = null;
            try
            {
                madeShot = game.MakeSampleShotOnEmptySquareForPlayer2();
            }
            catch (BattleshipGameException ex)
            {
                exception = ex;
            }

            madeShot.ShouldBeNull();
            game.Player2Board.Shots.Count.ShouldBe(0);
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<BattleshipGameException>();
            exception.Message.ShouldBe("Unable to make a shot when game is not started");
        }

        [Fact]
        public void WhenAllPlayersHasPlacedAllShips_AndMakeShotToEmptySquare_ThenResultIsMissingAndGameIsPending()
        {
            IBattleshipGame game = GameExtensions.CreateStandardGame();

            game.PreparePlayer1BoardToGame(SAMPLE_PLAYER1_NAME);
            game.PreparePlayer2BoardToGame(SAMPLE_PLAYER2_NAME);

            Shot madeShot = game.MakeSampleShotOnEmptySquareForPlayer2();

            madeShot.ShouldNotBeNull();
            madeShot.Result.ShouldBe(ShotResult.Miss);
            game.GetCurrentStatus().ShouldBe(BattleshipGameStatus.DuringGame);
        }

        [Fact]
        public void WhenAllPlayersHasPlacedAllShips_AndMakeShotToSquareWithPlacedPartOfShip_ThenResultIsHitAndGameIsPending()
        {
            IBattleshipGame game = GameExtensions.CreateStandardGame();

            game.PreparePlayer1BoardToGame(SAMPLE_PLAYER1_NAME);
            game.PreparePlayer2BoardToGame(SAMPLE_PLAYER2_NAME);

            Shot madeShot = game.MakeSampleShotOnSquareWithPart1OfSampleDestroyer2ForPlayer2();

            madeShot.ShouldNotBeNull();
            madeShot.Result.ShouldBe(ShotResult.Hit);
            game.GetCurrentStatus().ShouldBe(BattleshipGameStatus.DuringGame);
        }

        [Fact]
        public void WhenAllPlayersHasPlacedAllShips_AndMakeShotToSquareWithPlacedLastPartOfShip_ThenResultIsHitAndSinkAndGameIsPending()
        {
            IBattleshipGame game = GameExtensions.CreateStandardGame();

            game.PreparePlayer1BoardToGame(SAMPLE_PLAYER1_NAME);
            game.PreparePlayer2BoardToGame(SAMPLE_PLAYER2_NAME);

            game.MakeSampleShotOnSquareWithPart1OfSampleDestroyer2ForPlayer2();
            game.MakeSampleShotOnSquareWithPart2OfSampleDestroyer2ForPlayer2();
            game.MakeSampleShotOnSquareWithPart3OfSampleDestroyer2ForPlayer2();

            Shot madeShot = game.MakeSampleShotOnSquareWithPart4OfSampleDestroyer2ForPlayer2();

            madeShot.ShouldNotBeNull();
            madeShot.Result.ShouldBe(ShotResult.HitAndSink);
            game.GetCurrentStatus().ShouldBe(BattleshipGameStatus.DuringGame);
        }

        [Fact]
        public void WhenAllPlayersHasPlacedAllShips_AndMakeShotToSameSquare_ThenThrowsExpcetion()
        {
            IBattleshipGame game = GameExtensions.CreateStandardGame();

            game.PreparePlayer1BoardToGame(SAMPLE_PLAYER1_NAME);
            game.PreparePlayer2BoardToGame(SAMPLE_PLAYER2_NAME);

            game.MakeSampleShotOnSquareWithPart1OfSampleDestroyer2ForPlayer2();

            Shot madeShot = null;
            Exception exception = null;
            try
            {
                madeShot = game.MakeSampleShotOnSquareWithPart1OfSampleDestroyer2ForPlayer2();
            }
            catch (BattleshipGameException ex)
            {
                exception = ex;
            }

            madeShot.ShouldBeNull();
            game.Player2Board.Shots.Count.ShouldBe(1);
            exception.ShouldNotBeNull();
            exception.ShouldBeOfType<BattleshipGameException>();
            exception.Message.ShouldBe("Unable to make a shot, because this square has been already shooted");
        }

        [Fact]
        public void WhenAllPlayersHasPlacedAllShips_AndMakeShotToSquareWithPlacedLastPartOfLastShip_ThenResultIsHitAndSinkAndGameEnds()
        {
            IBattleshipGame game = GameExtensions.CreateStandardGame();

            game.PreparePlayer1BoardToGame(SAMPLE_PLAYER1_NAME);
            game.PreparePlayer2BoardToGame(SAMPLE_PLAYER2_NAME);

            game.MakeSampleShotOnSquareWithPart1OfSampleDestroyer1ForPlayer2();
            game.MakeSampleShotOnSquareWithPart2OfSampleDestroyer1ForPlayer2();
            game.MakeSampleShotOnSquareWithPart3OfSampleDestroyer1ForPlayer2();
            game.MakeSampleShotOnSquareWithPart4OfSampleDestroyer1ForPlayer2();
            game.MakeSampleShotOnSquareWithPart1OfSampleDestroyer2ForPlayer2();
            game.MakeSampleShotOnSquareWithPart2OfSampleDestroyer2ForPlayer2();
            game.MakeSampleShotOnSquareWithPart3OfSampleDestroyer2ForPlayer2();
            game.MakeSampleShotOnSquareWithPart4OfSampleDestroyer2ForPlayer2();
            game.MakeSampleShotOnSquareWithPart1OfSampleBattleship1ForPlayer2();
            game.MakeSampleShotOnSquareWithPart2OfSampleBattleship1ForPlayer2();
            game.MakeSampleShotOnSquareWithPart3OfSampleBattleship1ForPlayer2();
            game.MakeSampleShotOnSquareWithPart4OfSampleBattleship1ForPlayer2();

            Shot madeShot = game.MakeSampleShotOnSquareWithPart5OfSampleBattleship1ForPlayer2();

            madeShot.ShouldNotBeNull();
            madeShot.Result.ShouldBe(ShotResult.HitAndSink);
            game.GetCurrentStatus().ShouldBe(BattleshipGameStatus.Finished);
            game.Player2Board.IsWinner().ShouldBeTrue();
        }
    }
}
