﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using Battleship.Core.Game;
using Battleship.Core.Game.Factories;
using Battleship.Core.Game.Enums;
using System.Linq;
using Battleship.Core.Game.Validators;

namespace Battleship.Tests.Game
{
    public static class ComputerPlayerTestsExtensions
    {
        private static IBattleshipRulesFactory _battleshipRulesFactory { get { return new BattleshipRulesFactory(); } }
        private static ICoordinatesFactory _coordinatesFactory { get { return new CoordinatesFactory(); } }
        private static IShipValidator _shipValidator { get { return new ShipValidator(); } }
        private static IShotValidator _shotValidator { get { return new ShotValidator(); } }
        private static IShipFactory _shipFactory { get { return new ShipFactory(_coordinatesFactory, _shipValidator); } }

        public static IBattleshipBoard CreateStandardBoard()
        {
            BattleshipRules standardRules = _battleshipRulesFactory.CreateRulesForTwoPlayersMode();

            IBattleshipBoard playerBoard = new BattleshipBoard(_shipValidator, _shotValidator, standardRules);
            return playerBoard;
        }

        public static bool PlaceRandomShipOnNotOccupiedSquares(this IBattleshipBoard playerBoard)
        {
            Ship randomShip = _shipFactory.CreateRandomShipOnNotOccupiedSquares(playerBoard);

            bool isRandomShipPlaced = playerBoard.PlaceShip(randomShip);
            return isRandomShipPlaced;
        }

        public static bool PlaceAllRandomShipsFollowingRulesOnNotOccupiedSquares(this IBattleshipBoard playerBoard)
        {
            List<Ship> allRandomShips = _shipFactory.CreateAllRandomShipsFollowingRules(playerBoard);

            bool areRandomShipPlaced = playerBoard.PlaceShips(allRandomShips);
            return areRandomShipPlaced;
        }
    }

    public class ComputerPlayerTests
    {
        [Fact]
        public void WhenPlayerIsComputer_AndPlaceShipAtRandomosition_ThenShipIsAdded()
        {
            IBattleshipBoard playerBoard = ComputerPlayerTestsExtensions.CreateStandardBoard();

            bool isRandomShipPlaced = playerBoard.PlaceRandomShipOnNotOccupiedSquares();

            isRandomShipPlaced.ShouldBeTrue();
            playerBoard.GetShipsCount().ShouldBe(1);
        }

        [Fact]
        public void WhenPlayerIsComputer_AndPlaceAllShipsFollowingRulesAtRandomosition_ThenShipsAreAdded()
        {
            IBattleshipBoard playerBoard = ComputerPlayerTestsExtensions.CreateStandardBoard();

            bool areRandomShipPlaced = playerBoard.PlaceAllRandomShipsFollowingRulesOnNotOccupiedSquares();

            areRandomShipPlaced.ShouldBeTrue();
            playerBoard.GetShipsCount().ShouldBe(playerBoard.Rules.ShipsToPlace.Sum(@as => @as.Value));
        }

        [Fact]
        public void WhenPlayerIsComputer_AndPlaceAllShipsFollowingRulesAtRandomosition_ThenPlayerIsrReadyToStartGame()
        {
            IBattleshipBoard playerBoard = ComputerPlayerTestsExtensions.CreateStandardBoard();

            bool areRandomShipPlaced = playerBoard.PlaceAllRandomShipsFollowingRulesOnNotOccupiedSquares();
            
            playerBoard.GetCurrentStatus().ShouldBe(BattleshipBoardStatus.DuringGame);
        }
    }
}
