﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game.Exceptions
{
    public class BattleshipGameException : Exception
    {
        public BattleshipGameException(string msg) : base(msg) { }
    }
}
