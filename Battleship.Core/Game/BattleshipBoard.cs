﻿using Battleship.Core.Game.Enums;
using Battleship.Core.Game.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Battleship.Core.Game.Validators;

namespace Battleship.Core.Game
{
    public interface IBattleshipBoard
    {
        BattleshipRules Rules { get; }

        string PlayerName { get; }

        List<Ship> Ships { get; }
        List<Shot> Shots { get; }

        void SetPlayerName(string playerName);

        int GetShipsCount();
        bool IsEmpty();
        bool PlaceShip(Ship ship);
        bool PlaceShips(List<Ship> ships);
        BattleshipBoardStatus GetCurrentStatus();

        Shot MakeShot(CoordinatesVO shotPosition, IBattleshipBoard opponentBoard);

        bool IsWinner();
    }

    public class BattleshipBoard : IBattleshipBoard
    {
        private readonly IShipValidator _shipValidator;
        private readonly IShotValidator _shotValidator;

        public BattleshipRules Rules { get; private set; }

        public string PlayerName { get; private set; }

        public List<Ship> Ships { get; private set; }
        public List<Shot> Shots { get; private set; }

        public BattleshipBoard(IShipValidator shipValidator, IShotValidator shotValidator,
            BattleshipRules rules)
        {
            this._shipValidator = shipValidator;
            this._shotValidator = shotValidator;

            this.Rules = rules;

            this.Ships = new List<Ship>();
            this.Shots = new List<Shot>();
        }

        public void SetPlayerName(string playerName)
        {
            this.PlayerName = playerName;
        }

        public int GetShipsCount()
        {
            int shipsCount = this.Ships.Count;
            return shipsCount;
        }

        public bool IsEmpty()
        {
            bool isEmpty = (this.GetShipsCount() == 0);
            return isEmpty;
        }

        public bool PlaceShip(Ship ship)
        {
            bool shipPlaced;

            if (this._shipValidator.CanPlaceShip(ship, this.Rules, alreadyPlacedShips: this.Ships))
            {
                this.Ships.Add(ship);
                shipPlaced = true;
            }
            else
                shipPlaced = false;

            return shipPlaced;
        }

        public bool PlaceShips(List<Ship> shipsToPlace)
        {
            List<Ship> placedShips = new List<Ship>();
            foreach (Ship ship in shipsToPlace)
            {
                bool isShipPlaced = this.PlaceShip(ship);
                if (isShipPlaced)
                    placedShips.Add(ship);
            }

            bool allShipsPlaced = (placedShips.Count == shipsToPlace.Count);
            return allShipsPlaced;
        }

        private bool AreAllShipsPlaced()
        {
            bool areAllShipsPlaced = this.Rules.ShipsToPlace
                .All(@as => @as.Value == this.Ships.Where(s => s.Type == @as.Key).Count());

            return areAllShipsPlaced;
        }

        private bool AreAllShipsHitAndSunk()
        {
            List<Ship> hitAndSinkShips = this.Shots.Where(s => s.Result == ShotResult.HitAndSink)
                .Select(s => s.ShipHit)
                .ToList();

            bool areAllShipsHitAndSunk = this.Rules.ShipsToShot.Any() && this.Rules.ShipsToShot
                .All(@as => @as.Value == hitAndSinkShips.Where(s => s.Type == @as.Key).Count());

            return areAllShipsHitAndSunk;
        }

        public BattleshipBoardStatus GetCurrentStatus()
        {
            BattleshipBoardStatus currentStatus;

            bool areAllShipsPlaced = this.AreAllShipsPlaced();
            if (areAllShipsPlaced)
            {
                bool areAllShipsHitAndSunk = this.AreAllShipsHitAndSunk();
                if (areAllShipsHitAndSunk)
                    currentStatus = BattleshipBoardStatus.GameFinished;
                else
                    currentStatus = BattleshipBoardStatus.DuringGame;
            }
            else
                currentStatus = BattleshipBoardStatus.DuringPreparation;

            return currentStatus;
        }

        private Ship GetShipHit(CoordinatesVO shotPosition, IBattleshipBoard opponentBoard)
        {
            Ship shipHit = opponentBoard.Ships.Where(s => s.GetOccupiedCoordinates().Contains(shotPosition)).SingleOrDefault();
            return shipHit;
        }

        private List<CoordinatesVO> GetShipShotsPositions(CoordinatesVO shotPosition, Ship shipHit)
        {
            List<CoordinatesVO> shipShotsPositions = this.Shots
                .Where(s => s.ShipHit != null && s.ShipHit.Equals(shipHit))
                .Select(s => s.Position)
                .ToList();

            shipShotsPositions.Add(shotPosition);

            return shipShotsPositions;
        }

        public Shot MakeShot(CoordinatesVO shotPosition, IBattleshipBoard opponentBoard)
        {
            Shot shot = Shot.Empty();

            if (this._shotValidator.CanMakeShot(shotPosition, playerBoard: this, opponentBoard: opponentBoard))
            {
                ShotResult shotResult;

                Ship shipHit = this.GetShipHit(shotPosition, opponentBoard);
                if (shipHit != null)
                {
                    List<CoordinatesVO> coordinatesOccupiedByShip = shipHit.GetOccupiedCoordinates();
                    List<CoordinatesVO> shipShotsPositions = this.GetShipShotsPositions(shotPosition, shipHit);

                    if (shipShotsPositions.Count < coordinatesOccupiedByShip.Count)
                        shotResult = ShotResult.Hit;
                    else
                        shotResult = ShotResult.HitAndSink;
                }
                else
                    shotResult = ShotResult.Miss;

                shot = new Shot(shotPosition, shipHit, shotResult);
                this.Shots.Add(shot);
            }

            return shot;
        }

        public bool IsWinner()
        {
            bool isWinner = this.AreAllShipsHitAndSunk();
            return isWinner;
        }
    }
}
