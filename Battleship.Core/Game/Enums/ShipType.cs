﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game.Enums
{
    public enum ShipType
    {
        Destroyer = 4,
        Battleship = 5
    }
}
