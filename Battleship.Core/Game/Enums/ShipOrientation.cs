﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game.Enums
{
    public enum ShipOrientation
    {
        Horizontal,
        Vertical
    }
}
