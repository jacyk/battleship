﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game.Enums
{
    public enum ShotResult
    {
        Unknown,

        Miss,
        Hit,
        HitAndSink
    }
}
