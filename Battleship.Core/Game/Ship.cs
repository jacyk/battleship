﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Battleship.Core.Game
{
    public class Ship
    {
        public CoordinatesVO StartPosition { get; set; }
        public ShipType Type { get; set; }
        public ShipOrientation Orientation { get; set; }

        public Ship(CoordinatesVO startPosition, ShipType type, ShipOrientation orientation)
        {
            this.StartPosition = startPosition;
            this.Type = type;
            this.Orientation = orientation;
        }

        public List<CoordinatesVO> GetOccupiedCoordinates()
        {
            int coordinatesOccupiedByShipCount = (int)this.Type;

            List<CoordinatesVO> occupiedCoordinates = new List<CoordinatesVO>();

            for (int i = 0; i < coordinatesOccupiedByShipCount; i++)
            {
                CoordinatesVO occupiedCoordinate;

                if (this.Orientation == ShipOrientation.Horizontal)
                    occupiedCoordinate = this.StartPosition.Add(x: (short)i, y: 0);
                else
                    occupiedCoordinate = this.StartPosition.Add(x: 0, y: (short)i);

                occupiedCoordinates.Add(occupiedCoordinate);
            }

            return occupiedCoordinates;
        }

        private List<CoordinatesVO> GetReservedCoordinates(CoordinatesVO coordinate)
        {
            List<CoordinatesVO> reservedCoordinates = new List<CoordinatesVO>()
            {
                coordinate.Add(x: -1, y: -1),
                coordinate.Add(x: -1, y: 0),
                coordinate.Add(x: -1, y: 1),

                coordinate.Add(x: 0, y: -1),
                coordinate.Add(x: 0, y: 1),

                coordinate.Add(x: 1, y: -1),
                coordinate.Add(x: 1, y: 0),
                coordinate.Add(x: 1, y: 1),
            };

            return reservedCoordinates;
        }

        public List<CoordinatesVO> GetOccupiedAndReservedCoordinates()
        {
            List<CoordinatesVO> occupiedCoordinates = this.GetOccupiedCoordinates();

            List<CoordinatesVO> occupiedAndReservedCoordinates = new List<CoordinatesVO>();
            foreach (CoordinatesVO occupiedCoordinate in occupiedCoordinates)
            {
                occupiedAndReservedCoordinates.Add(occupiedCoordinate);
                occupiedAndReservedCoordinates.AddRange(this.GetReservedCoordinates(occupiedCoordinate));
            }

            occupiedAndReservedCoordinates = occupiedAndReservedCoordinates.Distinct().ToList();

            return occupiedAndReservedCoordinates;
        }
    }
}
