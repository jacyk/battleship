﻿using Battleship.Core.Game.Enums;
using Battleship.Core.Game.Validators;
using Battleship.DataStorage;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game
{
    public interface IBattleshipGame
    {
        IBattleshipBoard Player1Board { get; }
        IBattleshipBoard Player2Board { get; }

        IBattleshipGame SetFirstPlayerBoard(string playerName, BattleshipRules rules);
        IBattleshipGame SetSecondPlayerBoard(string playerName, BattleshipRules rules);

        BattleshipGameStatus GetCurrentStatus();
    }

    public class BattleshipGame : IBattleshipGame, IEntity<Guid>
    {
        private readonly IShipValidator _shipValidator;
        private readonly IShotValidator _shotValidator;

        public Guid Id { get; set; }

        public IBattleshipBoard Player1Board { get; private set; }
        public IBattleshipBoard Player2Board { get; private set; }

        public BattleshipGame(IShipValidator shipValidator, IShotValidator shotValidator)
        {
            this._shipValidator = shipValidator;
            this._shotValidator = shotValidator;

            this.Id = Guid.NewGuid();
        }

        public IBattleshipGame SetFirstPlayerBoard(string playerName, BattleshipRules rules)
        {
            this.Player1Board = new BattleshipBoard(this._shipValidator, this._shotValidator, rules);
            this.Player1Board.SetPlayerName(playerName);

            return this;
        }

        public IBattleshipGame SetSecondPlayerBoard(string playerName, BattleshipRules rules)
        {

            this.Player2Board = new BattleshipBoard(this._shipValidator, this._shotValidator, rules);
            this.Player2Board.SetPlayerName(playerName);

            return this;
        }

        public BattleshipGameStatus GetCurrentStatus()
        {
            BattleshipGameStatus currentStatus;

            BattleshipBoardStatus player1Status = this.Player1Board.GetCurrentStatus();
            BattleshipBoardStatus player2Status = this.Player2Board.GetCurrentStatus();

            bool isGameFinished = (player1Status == BattleshipBoardStatus.GameFinished || player2Status == BattleshipBoardStatus.GameFinished);
            if (isGameFinished)
                currentStatus = BattleshipGameStatus.Finished;
            else
            {
                bool isAnyOfPlayerDuringPreparation = (player1Status == BattleshipBoardStatus.DuringPreparation || player2Status == BattleshipBoardStatus.DuringPreparation);
                if (isAnyOfPlayerDuringPreparation)
                    currentStatus = BattleshipGameStatus.DuringPreparation;
                else
                    currentStatus = BattleshipGameStatus.DuringGame;
            }

            return currentStatus;
        }
    }
}
