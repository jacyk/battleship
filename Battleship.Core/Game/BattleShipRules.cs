﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game
{
    public class BattleshipRules
    {
        public char XcoordinateMin { get; set; }
        public char XcoordinateMax { get; set; }

        public short YcoordinateMin { get; set; }
        public short YcoordinateMax { get; set; }

        public bool CanPlaceShips { get; set; }
        public List<KeyValuePair<ShipType, int>> ShipsToPlace { get; set; }
        public bool CanShotShips { get; set; }
        public List<KeyValuePair<ShipType, int>> ShipsToShot { get; set; }

        public BattleshipRules(char xCoordinateMin, char xCoordinateMax,
            short yCoordinateMin, short yCoordinateMax,
            List<KeyValuePair<ShipType, int>> shipsToPlace,
            List<KeyValuePair<ShipType, int>> shipsToShot
        )
        {
            this.XcoordinateMin = xCoordinateMin;
            this.XcoordinateMax = xCoordinateMax;

            this.YcoordinateMin = yCoordinateMin;
            this.YcoordinateMax = yCoordinateMax;

            this.ShipsToPlace = shipsToPlace;
            this.ShipsToShot = shipsToShot;
        }

        public List<char> GetXlist()
        {
            List<char> xlist = new List<char>();

            for (char x = this.XcoordinateMin; x <= this.XcoordinateMax; x++)
            {
                xlist.Add(x);
            }

            return xlist;
        }

        public List<short> GetYlist()
        {
            List<short> ylist = new List<short>();

            for (short x = this.YcoordinateMin; x <= this.YcoordinateMax; x++)
            {
                ylist.Add(x);
            }

            return ylist;
        }

        public List<CoordinatesVO> GetAllPossibleCoordinates()
        {
            List<CoordinatesVO> allPossibleCoordinates = new List<CoordinatesVO>();
            foreach (char x in this.GetXlist())
            {
                foreach (short y in this.GetYlist())
                {
                    CoordinatesVO possibleCoordinate = new CoordinatesVO(x, y);
                    allPossibleCoordinates.Add(possibleCoordinate);
                }
            }

            return allPossibleCoordinates;
        }
    }
}
