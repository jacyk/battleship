﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game.Factories
{
    public interface ICoordinatesFactory
    {
        CoordinatesVO Create(char x, short y);
        CoordinatesVO Create(string xy);
    }

    public class CoordinatesFactory : ICoordinatesFactory
    {
        public CoordinatesVO Create(char x, short y)
        {
            CoordinatesVO coordinates = new CoordinatesVO(x, y);
            return coordinates;
        }

        public CoordinatesVO Create(string xy)
        {
            if (!String.IsNullOrEmpty(xy) && xy.Length >= 2 && xy.Length <= 3)
            {
                char x = Convert.ToChar(xy.Substring(startIndex: 0, length: 1));
                short y = Convert.ToInt16(xy.Substring(startIndex: 1));

                CoordinatesVO coordinates = this.Create(x, y); ;
                return coordinates;
            }
            else
                throw new ArgumentException("Invalid format of XY- X should be a letter (A-Z) and Y should be number (1-10)");
        }
    }
}
