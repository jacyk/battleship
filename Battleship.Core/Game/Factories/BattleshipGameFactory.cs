﻿using Battleship.Core.Game.Validators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game.Factories
{
    public interface IBattleshipGameFactory
    {
        BattleshipGame CreateGame();
        BattleshipGame CreateStandardGame();
        BattleshipGame CreateStandardGameWithComputerPlayer(string playerName);
    }

    public class BattleshipGameFactory : IBattleshipGameFactory
    {
        private readonly IBattleshipRulesFactory _rulesFactory;
        private readonly IShipValidator _shipValidator;
        private readonly IShotValidator _shotValidator;
        private readonly IShipFactory _shipFactory;

        public BattleshipGameFactory(IBattleshipRulesFactory rulesFactory, IShipValidator shipValidator, IShotValidator shotValidator, IShipFactory shipFactory)
        {
            this._rulesFactory = rulesFactory;
            this._shipValidator = shipValidator;
            this._shotValidator = shotValidator;
            this._shipFactory = shipFactory;
        }

        public BattleshipGame CreateGame()
        {
            BattleshipGame game = new BattleshipGame(this._shipValidator, this._shotValidator);
            return game;
        }

        public BattleshipGame CreateStandardGame()
        {
            BattleshipGame standardGame = this.CreateGame();
            return standardGame;
        }

        public BattleshipGame CreateStandardGameWithComputerPlayer(string playerName)
        {
            BattleshipGame standardGame = this.CreateStandardGame();

            BattleshipRules rulesForComputerPlayer = this._rulesFactory.CreateRulesForComputerPlayer();
            standardGame.SetFirstPlayerBoard(playerName: "Computer", rules: rulesForComputerPlayer);
            List<Ship> randomShipsOfComputerPlayer = this._shipFactory.CreateAllRandomShipsFollowingRules(standardGame.Player1Board);
            standardGame.Player1Board.PlaceShips(randomShipsOfComputerPlayer);

            BattleshipRules rulesForHumanPlayer = this._rulesFactory.CreateRulesForHumanPlayer();
            standardGame.SetSecondPlayerBoard(playerName, rules: rulesForHumanPlayer);

            return standardGame;
        }
    }
}
