﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game.Factories
{
    public interface IBattleshipRulesFactory
    {
        BattleshipRules CreateRulesForComputerPlayer();
        BattleshipRules CreateRulesForHumanPlayer();

        BattleshipRules CreateRulesForTwoPlayersMode();
    }

    public class BattleshipRulesFactory : IBattleshipRulesFactory
    {
        public BattleshipRules CreateRulesForComputerPlayer()
        {
            BattleshipRules rulesForComputerPlayer = new BattleshipRules(xCoordinateMin: 'A', xCoordinateMax: 'J',
                yCoordinateMin: 1, yCoordinateMax: 10,
                shipsToPlace: new List<KeyValuePair<ShipType, int>>()
                {
                    new KeyValuePair<ShipType, int>(ShipType.Destroyer, 2),
                    new KeyValuePair<ShipType, int>(ShipType.Battleship, 1)
                },
                shipsToShot: new List<KeyValuePair<ShipType, int>>()
            );

            return rulesForComputerPlayer;
        }

        public BattleshipRules CreateRulesForHumanPlayer()
        {
            BattleshipRules rulesForHumanPlayer = new BattleshipRules(xCoordinateMin: 'A', xCoordinateMax: 'J',
                yCoordinateMin: 1, yCoordinateMax: 10,
                shipsToPlace: new List<KeyValuePair<ShipType, int>>(),
                shipsToShot: new List<KeyValuePair<ShipType, int>>()
                {
                    new KeyValuePair<ShipType, int>(ShipType.Destroyer, 2),
                    new KeyValuePair<ShipType, int>(ShipType.Battleship, 1)
                }
            );

            return rulesForHumanPlayer;
        }

        public BattleshipRules CreateRulesForTwoPlayersMode()
        {
            BattleshipRules rulesForTwoPlayersMode = new BattleshipRules(xCoordinateMin: 'A', xCoordinateMax: 'J',
                yCoordinateMin: 1, yCoordinateMax: 10,
                shipsToPlace: new List<KeyValuePair<ShipType, int>>()
                {
                    new KeyValuePair<ShipType, int>(ShipType.Destroyer, 2),
                    new KeyValuePair<ShipType, int>(ShipType.Battleship, 1)
                },
                shipsToShot: new List<KeyValuePair<ShipType, int>>()
                {
                    new KeyValuePair<ShipType, int>(ShipType.Destroyer, 2),
                    new KeyValuePair<ShipType, int>(ShipType.Battleship, 1)
                }
            );

            return rulesForTwoPlayersMode;
        }
    }
}
