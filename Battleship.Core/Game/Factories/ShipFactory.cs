﻿using Battleship.Core.Game.Enums;
using Battleship.Core.Game.Validators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Battleship.Core.Game.Exceptions;

namespace Battleship.Core.Game.Factories
{
    public interface IShipFactory
    {
        Ship CreateShip(string startPosition, ShipType shipType, ShipOrientation orientation);
        Ship CreateShip(CoordinatesVO startPosition, ShipType shipType, ShipOrientation orientation);

        Ship CreateRandomShipOnNotOccupiedSquares(IBattleshipBoard playerBoard);
        Ship CreateRandomShipOnNotOccupiedSquares(IBattleshipBoard playerBoard, ShipType shipType, ShipOrientation shipOrientation);
        List<Ship> CreateAllRandomShipsFollowingRules(IBattleshipBoard playerBoard);
    }

    public class ShipFactory : IShipFactory
    {
        private readonly Random _random;
        private readonly ICoordinatesFactory _coordinatesFactory;
        private readonly IShipValidator _shipValidator;

        public ShipFactory(ICoordinatesFactory coordinatesFactory, IShipValidator shipValidator)
        {
            this._random = new Random();

            this._coordinatesFactory = coordinatesFactory;
            this._shipValidator = shipValidator;
        }

        public Ship CreateShip(string startPosition, ShipType shipType, ShipOrientation orientation)
        {
            CoordinatesVO startPositionCoordinates = this._coordinatesFactory.Create(startPosition);

            Ship ship = this.CreateShip(startPositionCoordinates, shipType, orientation);
            return ship;
        }

        public Ship CreateShip(CoordinatesVO startPosition, ShipType shipType, ShipOrientation orientation)
        {
            Ship ship = new Ship(startPosition, shipType, orientation);
            return ship;
        }

        private int GenerateRandomIdx<T>(IEnumerable<T> list)
        {
            int randomIdx = this._random.Next(minValue: 0, maxValue: list.Count());
            return randomIdx;
        }

        private ShipType GenerateRandomShipTypeWhichIsToPlace(IBattleshipBoard playerBoard)
        {
            List<ShipType> availableShipTypes = playerBoard.Rules.ShipsToPlace
                .Select(@as => @as.Key)
                .Except(playerBoard.Ships.Select(s => s.Type))
                .ToList();

            int randomIdx = this.GenerateRandomIdx(availableShipTypes);
            ShipType randomShipType = availableShipTypes[randomIdx];
            return randomShipType;
        }

        private ShipOrientation GenerateRandomShipOrientation()
        {
            List<ShipOrientation> shipOrientations = Enum.GetValues(typeof(ShipOrientation)).Cast<ShipOrientation>().ToList();

            int randomIdx = this.GenerateRandomIdx(shipOrientations);
            ShipOrientation randomShipOrientation = shipOrientations[randomIdx];
            return randomShipOrientation;
        }

        private CoordinatesVO GenerateRandomShipPositionOnNotOccupiedSquares(List<CoordinatesVO> allAvailableCoordinates, List<Ship> placedShips, List<CoordinatesVO> coordinatesToOmmit)
        {
            List<CoordinatesVO> occupiedCoordinates = placedShips
                .SelectMany(s => s.GetOccupiedAndReservedCoordinates())
                .Distinct()
                .ToList();

            List<CoordinatesVO> notOccupiedSquares = allAvailableCoordinates
                .Except(occupiedCoordinates)
                .Except(coordinatesToOmmit)
                .ToList();

            int randomIdx = this.GenerateRandomIdx(notOccupiedSquares);
            CoordinatesVO randomShipStartPosition = notOccupiedSquares[randomIdx];
            return randomShipStartPosition;
        }

        public Ship CreateRandomShipOnNotOccupiedSquares(IBattleshipBoard playerBoard)
        {
            ShipType randomShipType = this.GenerateRandomShipTypeWhichIsToPlace(playerBoard);
            ShipOrientation randomShipOrientation = this.GenerateRandomShipOrientation();

            Ship randomShip = this.CreateRandomShipOnNotOccupiedSquares(playerBoard, randomShipType, randomShipOrientation);
            return randomShip;
        }

        public Ship CreateRandomShipOnNotOccupiedSquares(BattleshipRules rules, List<CoordinatesVO> allAvailableCoordinates, List<Ship> placedShips, ShipType shipType, ShipOrientation shipOrientation)
        {
            Ship randomShip;
            bool isRandomShipValid;

            List<CoordinatesVO> invalidShipStartPositions = new List<CoordinatesVO>();
            do
            {
                CoordinatesVO randomShipStartPosition = this.GenerateRandomShipPositionOnNotOccupiedSquares(allAvailableCoordinates, placedShips, invalidShipStartPositions);
                randomShip = this.CreateShip(randomShipStartPosition, shipType, shipOrientation);

                try
                {
                    isRandomShipValid = this._shipValidator.CanPlaceShip(randomShip, rules, placedShips);
                    if (!isRandomShipValid)
                        invalidShipStartPositions.Add(randomShipStartPosition);
                }
                catch (BattleshipGameException e)
                {
                    isRandomShipValid = false;
                    invalidShipStartPositions.Add(randomShipStartPosition);
                }
            }
            while (!isRandomShipValid);

            return randomShip;
        }

        public Ship CreateRandomShipOnNotOccupiedSquares(IBattleshipBoard playerBoard, ShipType shipType, ShipOrientation shipOrientation)
        {
            Ship randomShip = this.CreateRandomShipOnNotOccupiedSquares(playerBoard.Rules, playerBoard.Rules.GetAllPossibleCoordinates(), placedShips: playerBoard.Ships, shipType: shipType, shipOrientation: shipOrientation);
            return randomShip;
        }

        public List<Ship> CreateAllRandomShipsFollowingRules(IBattleshipBoard playerBoard)
        {
            List<CoordinatesVO> allAvailableCoordinates = playerBoard.Rules.GetAllPossibleCoordinates();
            List<Ship> allRandomShipsFollowingRules = new List<Ship>();

            foreach (KeyValuePair<ShipType, int> availableShip in playerBoard.Rules.ShipsToPlace)
            {
                for (int i = 0; i < availableShip.Value; i++)
                {
                    ShipOrientation randomShipOrientation = this.GenerateRandomShipOrientation();

                    Ship randomShip = this.CreateRandomShipOnNotOccupiedSquares(playerBoard.Rules, allAvailableCoordinates, placedShips: allRandomShipsFollowingRules, shipType: availableShip.Key, shipOrientation: randomShipOrientation);

                    allRandomShipsFollowingRules.Add(randomShip);
                    allAvailableCoordinates = allAvailableCoordinates.Except(randomShip.GetOccupiedAndReservedCoordinates()).ToList();
                }
            }

            return allRandomShipsFollowingRules;
        }
    }
}
