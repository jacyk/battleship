﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Battleship.Core.Game.Exceptions;

namespace Battleship.Core.Game.Validators
{
    public interface IShotValidator
    {
        bool CanMakeShot(CoordinatesVO shotPosition, IBattleshipBoard playerBoard, IBattleshipBoard opponentBoard);
    }

    public class ShotValidator: IShotValidator
    {
        public bool CanMakeShot(CoordinatesVO shotPosition, IBattleshipBoard playerBoard, IBattleshipBoard opponentBoard)
        {
            bool canMakeShot;

            if ((opponentBoard.GetCurrentStatus() == BattleshipBoardStatus.DuringGame) && (playerBoard.GetCurrentStatus() == BattleshipBoardStatus.DuringGame))
            {
                bool isPositionAlreadyShooted = playerBoard.Shots.Where(s => s.Position.Equals(shotPosition)).Any();
                if (!isPositionAlreadyShooted)
                    canMakeShot = true;
                else
                    throw new BattleshipGameException("Unable to make a shot, because this square has been already shooted");
            }
            else
                throw new BattleshipGameException("Unable to make a shot when game is not started");

            return canMakeShot;
        }
    }
}
