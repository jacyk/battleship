﻿using Battleship.Core.Game.Enums;
using Battleship.Core.Game.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Battleship.Core.Game.Validators
{
    public interface IShipValidator
    {
        bool CanPlaceShip(Ship ship, BattleshipRules rules, List<Ship> alreadyPlacedShips);
    }

    public class ShipValidator : IShipValidator
    {
        private bool IsShipAvailableToPlace(Ship ship, BattleshipRules rules, List<Ship> alreadyPlacedShips)
        {
            bool isShipAvailable;
            KeyValuePair<ShipType, int> availableShipsCountForShipType = rules.ShipsToPlace.Where(@as => @as.Key == ship.Type).SingleOrDefault();

            if (!availableShipsCountForShipType.Equals(default(KeyValuePair<ShipType, int>)))
            {
                int maxShipsCountForShipType = availableShipsCountForShipType.Value;
                int currentShipsCountForShipType = alreadyPlacedShips.Where(s => s.Type == ship.Type).Count();

                if ((currentShipsCountForShipType + 1) <= maxShipsCountForShipType)
                    isShipAvailable = true;
                else
                    throw new BattleshipGameException($"Unable to place ship, because all available ships of type '{ship.Type}' are already placed on board");
            }
            else
            {
                isShipAvailable = false;

                throw new BattleshipGameException($"Unable to place ship, because this type of ship is not in rules");
            }

            return isShipAvailable;
        }

        private bool IsWholeShipInsideBoard(Ship ship, BattleshipRules rules)
        {
            List<CoordinatesVO> occupiedCoordinates = ship.GetOccupiedCoordinates();

            bool isWholeShipInsideBoard = occupiedCoordinates.All(oc =>
                (oc.X >= rules.XcoordinateMin && oc.X <= rules.XcoordinateMax)
                && (oc.Y >= rules.YcoordinateMin && oc.Y <= rules.YcoordinateMax)
            );

            if (!isWholeShipInsideBoard)
                throw new BattleshipGameException("Unable to place ship, because it can not fit on the board");

            return isWholeShipInsideBoard;
        }

        private bool IsShipOverlapWithOtherShip(Ship ship, List<Ship> alreadyPlacedShips)
        {
            List<CoordinatesVO> occupiedCoordinatesByAllPlacedShips = alreadyPlacedShips.SelectMany(s => s.GetOccupiedAndReservedCoordinates()).ToList();
            bool isShipOverlapWithOtherShip = ship.GetOccupiedCoordinates().Where(oc => occupiedCoordinatesByAllPlacedShips.Contains(oc)).Any();

            if (isShipOverlapWithOtherShip)
                throw new BattleshipGameException("Unable to place ship, because it overlap with other ship");

            return isShipOverlapWithOtherShip;
        }

        public bool CanPlaceShip(Ship ship, BattleshipRules rules, List<Ship> alreadyPlacedShips)
        {
            bool canPlaceShip = (this.IsShipAvailableToPlace(ship, rules, alreadyPlacedShips) && this.IsWholeShipInsideBoard(ship, rules) && !this.IsShipOverlapWithOtherShip(ship, alreadyPlacedShips));

            return canPlaceShip;
        }
    }
}
