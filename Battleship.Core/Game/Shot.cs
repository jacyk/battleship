﻿using Battleship.Core.Game.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game
{
    public class Shot
    {
        public CoordinatesVO Position { get; private set; }
        public Ship ShipHit { get; private set; }
        public ShotResult Result { get; set; }

        public Shot(CoordinatesVO position, ShotResult result)
        {
            this.Position = position;
            this.Result = result;
        }

        public Shot(CoordinatesVO position, Ship shipHit, ShotResult result)
            : this(position, result)
        {
            this.ShipHit = shipHit;
        }

        public static Shot Empty()
        {
            Shot emptyShot = new Shot(CoordinatesVO.Empty(), ShotResult.Unknown);
            return emptyShot;
        }
    }
}
