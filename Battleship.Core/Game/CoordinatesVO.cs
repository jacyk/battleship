﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Game
{
    public class CoordinatesVO
    {
        public char X { get; set; }
        public short Y { get; set; }

        public CoordinatesVO(char x, short y)
        {
            this.X = x;
            this.Y = y;
        }

        public CoordinatesVO Add(short x, short y)
        {
            CoordinatesVO newCoordinate = new CoordinatesVO(((char)((short)this.X + x)), ((short)(this.Y + y)));
            return newCoordinate;
        }

        public override bool Equals(object obj)
        {
            CoordinatesVO coordinate = obj as CoordinatesVO;

            bool isEqual = (coordinate != null && coordinate.X == this.X && coordinate.Y == this.Y);
            return isEqual;
        }

        public override int GetHashCode()
        {
            int hashCode = this.ToString().GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            string coordinatesStr = $"{this.X}{this.Y}";
            return coordinatesStr;
        }

        public static CoordinatesVO Empty()
        {
            CoordinatesVO emptyCoordinates = new CoordinatesVO(x: '_', y: 0);
            return emptyCoordinates;
        }
    }
}
